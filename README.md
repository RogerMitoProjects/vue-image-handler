# Vue Image Handler

This little code aims dismistify once and for all the image handling process into a Vue app. The idea is to summarize a general guide containing
tasks commonly performed, for instance:

- Render;
- Drag and drop;
- Proportional resize;
- Aspect ratio resize;

Due to its simplicity, the Vue CDN distribution was applied. I hope that from this code you can find out how simple, pratice and fun is this process :D (Forget your afraid)!

Below is shown the execution pipeline:

<img src="./img/main-flow.png">

Application aparence:

<img src="./img/main.png">

<h2>Drag and drop</h2>
Drag operations are very common in the web and mobile environments.
When we are talking about mobile devices, this feature may be something essential defining directly the usability level of an application. Drag and Drop feature provides facility and speed during the execution of some tasks, for example, items selection.

The code was built according HTML5 features that allow us to do a simple implementation using its native support to handle different media files (texts, images, videos, and so on).

<center>
<img src="./img/drag-and-drop.gif">
</center>

<h2>Active Resize</h2>

Active image resizing is a feature very special in a lot of projects that implement image upload. In this scenario, you probably have to compress the images to save our storage space, so you implement an image compression feature in the back-end. Well, you did it and saved your storage space!
But, unfortunately you didn't save your server :(
Image resizing takes several (and precious) server resources like CPU cycles and bandwidth. It may become a serious problem if you have a server with limited resources and has many tasks to run then you are just adding more CPU load.
So, is there some way to avoid that!? Yes, there is! How!?
Apply the image compression in the client using JavaScript!
Take advantage of the HTML5 Canvas that is used to draw graphics on web page. Canvas is just a container for your graphics, JavaScript is used to draw.

To get it, the code uses two techniques: the proportional resizing and the resizing based on display aspect. Both of them have the same basic idea and differ according its equations used for that.

Proportional resizing, as its name implies, uses a simple proportion to reduce the image size whereas the aspect ratio on the other hand, uses more accurate equations to reach that.

Proportiona resizing:

**imageHeight = h \* &#8333;h &#x2044; H&#8334;**

**imageWidth = w \* &#8333; w &#x2044; W &#8334;**

Above, h is the new height value and H is the original image height;
w is the new value for the width and W is the original image width.

The W and H values are pre-defined by application and commonly follow some resolution standard like 640x480p, 1024x768p and so on.

```javascript
// image scaling (proportionality)
const width = Math.round((MAX_WIDTH / img.width) * img.width);
const height = Math.round((MAX_HEIGHT / img.height) * img.height);
```

To get more details, please, take a look at the complete article: <a href="https://medium.com/@rogerio_oliveira1992/manipula%C3%A7%C3%A3o-e-renderiza%C3%A7%C3%A3o-de-imagens-com-vue-957dfe6857e2?source=friends_link&sk=e2472973522cfaf7fd4750b14eb6b317">https://medium.com/@rogerio_oliveira1992</a>
